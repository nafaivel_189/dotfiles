# My dotfiles

## screen

![look](.assets/hypr1.png)

## soft

| type    | soft                                                      | repo                                                                                          |
| ------- | --------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| wm      | [hyprland](./dots/.config/hypr)                           | [git](https://github.com/hyprwm/Hyprland), [docs](https://wiki.hyprland.org/)           |
| text    | [nvim](https://gitlab.com/Nafaivel/nvim_config)           | [git](https://github.com/neovim/neovim)                                                       |
| term    | [foot](./dots/.config/foot/)                              | [git mirror](https://github.com/DanteAlighierin/foot )                                        |
| fm      | [lf](./dots/.config/lf), [thunar](./dots/.config/Thunar/) | [lf git](https://github.com/gokcehan/lf), [thunar git](https://github.com/xfce-mirror/thunar) |
| browser | [qutebrowser](./dots/.config/qutebrowser/)                | [git](https://github.com/qutebrowser/qutebrowser)                                             |
| locker  | [swaylock](./dots/.config/wayland_scripts/lock.sh)        | [git](https://github.com/swaywm/swaylock)                                                     |
| menu    | [wofi](./dots/.config/wofi/)                              | [git](https://hg.sr.ht/~scoopta/wofi)                                                         |
| music   | [ncmpcpp](./dots/.config/ncmpcpp/)                        | [git](https://github.com/ncmpcpp/ncmpcpp)                                                     |
| shell   | [zsh](./dots/.zshrc)                                      | [site](https://www.zsh.org/)                                                                  |
| player  | [mpv](./dots/.config/mpv/)                                | [git](https://github.com/mpv-player/mpv)                                                      |
| reader  | [zathura](./dots/.config/zathura/)                        | [git](https://github.com/pwmt/zathura)                                                        |

## more screenshots

| ![./.assets/ri1.png](./.assets/ri1.png) | ![./.assets/aw1.png](./.assets/aw1.png) |
|-----------------------------------------|-----------------------------------------|
| ![./.assets/aw4.png](./.assets/aw4.png) | ![./.assets/aw2.png](./.assets/aw2.png) |
| ![./.assets/aw5.png](./.assets/aw5.png) | ![./.assets/aw3.png](./.assets/aw3.png) |

p.s. as you can see changes only colorscheme and wall
