#!/usr/bin/env bash
if [ -f "/bin/wal" ] && [ -v $EMC ] ; then
    # wal -R -t -q
    (cat ~/.cache/wal/sequences && printf %b '\e]11;#000000\a'&)
fi

$1
