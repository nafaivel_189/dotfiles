#!/usr/bin/env bash

while true; do
    res=$(ps aux | rg $1 | rg -v 'rg' | rg -v 'check' | wc -l)
    # echo $res
    if [[ $res -lt 1 ]] ; then
    notify-send -u critical "$1 is dead restart it"
    fi
    sleep 20
done
