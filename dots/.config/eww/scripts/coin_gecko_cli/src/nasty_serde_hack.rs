use serde::{Deserialize, Serialize, Serializer};
use num_format::{ToFormattedString, Locale};

// just add sikip if None
// but to do so i need to write this structure twice
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Price {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub btc: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub btc_market_cap: Option<f64>,
    #[serde(rename = "btc_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub btc24_h_vol: Option<f64>,
    #[serde(rename = "btc_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub btc24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eth: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eth_market_cap: Option<f64>,
    #[serde(rename = "eth_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eth24_h_vol: Option<f64>,
    #[serde(rename = "eth_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eth24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ltc: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ltc_market_cap: Option<f64>,
    #[serde(rename = "ltc_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ltc24_h_vol: Option<f64>,
    #[serde(rename = "ltc_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ltc24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bch: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bch_market_cap: Option<f64>,
    #[serde(rename = "bch_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bch24_h_vol: Option<f64>,
    #[serde(rename = "bch_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bch24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bnb: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bnb_market_cap: Option<f64>,
    #[serde(rename = "bnb_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bnb24_h_vol: Option<f64>,
    #[serde(rename = "bnb_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bnb24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eos: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eos_market_cap: Option<f64>,
    #[serde(rename = "eos_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eos24_h_vol: Option<f64>,
    #[serde(rename = "eos_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eos24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xrp: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xrp_market_cap: Option<f64>,
    #[serde(rename = "xrp_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xrp24_h_vol: Option<f64>,
    #[serde(rename = "xrp_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xrp24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xlm: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xlm_market_cap: Option<f64>,
    #[serde(rename = "xlm_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xlm24_h_vol: Option<f64>,
    #[serde(rename = "xlm_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xlm24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub link: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub link_market_cap: Option<f64>,
    #[serde(rename = "link_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub link24_h_vol: Option<f64>,
    #[serde(rename = "link_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub link24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dot: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dot_market_cap: Option<f64>,
    #[serde(rename = "dot_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dot24_h_vol: Option<f64>,
    #[serde(rename = "dot_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dot24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub yfi: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub yfi_market_cap: Option<f64>,
    #[serde(rename = "yfi_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub yfi24_h_vol: Option<f64>,
    #[serde(rename = "yfi_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub yfi24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub usd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub usd_market_cap: Option<f64>,
    #[serde(rename = "usd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub usd24_h_vol: Option<f64>,
    #[serde(rename = "usd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub usd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aed: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aed_market_cap: Option<f64>,
    #[serde(rename = "aed_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aed24_h_vol: Option<f64>,
    #[serde(rename = "aed_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aed24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ars: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ars_market_cap: Option<f64>,
    #[serde(rename = "ars_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ars24_h_vol: Option<f64>,
    #[serde(rename = "ars_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ars24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aud: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aud_market_cap: Option<f64>,
    #[serde(rename = "aud_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aud24_h_vol: Option<f64>,
    #[serde(rename = "aud_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub aud24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bdt: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bdt_market_cap: Option<f64>,
    #[serde(rename = "bdt_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bdt24_h_vol: Option<f64>,
    #[serde(rename = "bdt_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bdt24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bhd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bhd_market_cap: Option<f64>,
    #[serde(rename = "bhd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bhd24_h_vol: Option<f64>,
    #[serde(rename = "bhd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bhd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bmd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bmd_market_cap: Option<f64>,
    #[serde(rename = "bmd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bmd24_h_vol: Option<f64>,
    #[serde(rename = "bmd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bmd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub brl: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub brl_market_cap: Option<f64>,
    #[serde(rename = "brl_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub brl24_h_vol: Option<f64>,
    #[serde(rename = "brl_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub brl24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cad: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cad_market_cap: Option<f64>,
    #[serde(rename = "cad_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cad24_h_vol: Option<f64>,
    #[serde(rename = "cad_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cad24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub chf: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub chf_market_cap: Option<f64>,
    #[serde(rename = "chf_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub chf24_h_vol: Option<f64>,
    #[serde(rename = "chf_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub chf24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub clp: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub clp_market_cap: Option<f64>,
    #[serde(rename = "clp_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub clp24_h_vol: Option<f64>,
    #[serde(rename = "clp_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub clp24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cny: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cny_market_cap: Option<f64>,
    #[serde(rename = "cny_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cny24_h_vol: Option<f64>,
    #[serde(rename = "cny_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub cny24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub czk: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub czk_market_cap: Option<f64>,
    #[serde(rename = "czk_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub czk24_h_vol: Option<f64>,
    #[serde(rename = "czk_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub czk24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dkk: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dkk_market_cap: Option<f64>,
    #[serde(rename = "dkk_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dkk24_h_vol: Option<f64>,
    #[serde(rename = "dkk_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub dkk24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eur: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eur_market_cap: Option<f64>,
    #[serde(rename = "eur_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eur24_h_vol: Option<f64>,
    #[serde(rename = "eur_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub eur24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub gbp: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub gbp_market_cap: Option<f64>,
    #[serde(rename = "gbp_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub gbp24_h_vol: Option<f64>,
    #[serde(rename = "gbp_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub gbp24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub hkd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub hkd_market_cap: Option<f64>,
    #[serde(rename = "hkd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub hkd24_h_vol: Option<f64>,
    #[serde(rename = "hkd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub hkd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub huf: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub huf_market_cap: Option<f64>,
    #[serde(rename = "huf_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub huf24_h_vol: Option<f64>,
    #[serde(rename = "huf_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub huf24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub idr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub idr_market_cap: Option<f64>,
    #[serde(rename = "idr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub idr24_h_vol: Option<f64>,
    #[serde(rename = "idr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub idr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ils: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ils_market_cap: Option<f64>,
    #[serde(rename = "ils_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ils24_h_vol: Option<f64>,
    #[serde(rename = "ils_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ils24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub inr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub inr_market_cap: Option<f64>,
    #[serde(rename = "inr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub inr24_h_vol: Option<f64>,
    #[serde(rename = "inr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub inr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub jpy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub jpy_market_cap: Option<f64>,
    #[serde(rename = "jpy_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub jpy24_h_vol: Option<f64>,
    #[serde(rename = "jpy_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub jpy24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub krw: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub krw_market_cap: Option<f64>,
    #[serde(rename = "krw_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub krw24_h_vol: Option<f64>,
    #[serde(rename = "krw_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub krw24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub kwd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub kwd_market_cap: Option<f64>,
    #[serde(rename = "kwd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub kwd24_h_vol: Option<f64>,
    #[serde(rename = "kwd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub kwd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub lkr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub lkr_market_cap: Option<f64>,
    #[serde(rename = "lkr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub lkr24_h_vol: Option<f64>,
    #[serde(rename = "lkr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub lkr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mmk: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mmk_market_cap: Option<f64>,
    #[serde(rename = "mmk_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mmk24_h_vol: Option<f64>,
    #[serde(rename = "mmk_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mmk24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mxn: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mxn_market_cap: Option<f64>,
    #[serde(rename = "mxn_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mxn24_h_vol: Option<f64>,
    #[serde(rename = "mxn_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub mxn24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub myr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub myr_market_cap: Option<f64>,
    #[serde(rename = "myr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub myr24_h_vol: Option<f64>,
    #[serde(rename = "myr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub myr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ngn: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ngn_market_cap: Option<f64>,
    #[serde(rename = "ngn_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ngn24_h_vol: Option<f64>,
    #[serde(rename = "ngn_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub ngn24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nok: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nok_market_cap: Option<f64>,
    #[serde(rename = "nok_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nok24_h_vol: Option<f64>,
    #[serde(rename = "nok_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nok24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nzd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nzd_market_cap: Option<f64>,
    #[serde(rename = "nzd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nzd24_h_vol: Option<f64>,
    #[serde(rename = "nzd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub nzd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub php: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub php_market_cap: Option<f64>,
    #[serde(rename = "php_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub php24_h_vol: Option<f64>,
    #[serde(rename = "php_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub php24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pkr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pkr_market_cap: Option<f64>,
    #[serde(rename = "pkr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pkr24_h_vol: Option<f64>,
    #[serde(rename = "pkr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pkr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pln: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pln_market_cap: Option<f64>,
    #[serde(rename = "pln_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pln24_h_vol: Option<f64>,
    #[serde(rename = "pln_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub pln24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub rub: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub rub_market_cap: Option<f64>,
    #[serde(rename = "rub_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub rub24_h_vol: Option<f64>,
    #[serde(rename = "rub_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub rub24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sar: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sar_market_cap: Option<f64>,
    #[serde(rename = "sar_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sar24_h_vol: Option<f64>,
    #[serde(rename = "sar_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sar24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sek: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sek_market_cap: Option<f64>,
    #[serde(rename = "sek_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sek24_h_vol: Option<f64>,
    #[serde(rename = "sek_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sek24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sgd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sgd_market_cap: Option<f64>,
    #[serde(rename = "sgd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sgd24_h_vol: Option<f64>,
    #[serde(rename = "sgd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sgd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub thb: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub thb_market_cap: Option<f64>,
    #[serde(rename = "thb_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub thb24_h_vol: Option<f64>,
    #[serde(rename = "thb_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub thb24_h_change: Option<f64>,
    #[serde(rename = "try")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub try_field: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub try_market_cap: Option<f64>,
    #[serde(rename = "try_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub try24_h_vol: Option<f64>,
    #[serde(rename = "try_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub try24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub twd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub twd_market_cap: Option<f64>,
    #[serde(rename = "twd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub twd24_h_vol: Option<f64>,
    #[serde(rename = "twd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub twd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub uah: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub uah_market_cap: Option<f64>,
    #[serde(rename = "uah_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub uah24_h_vol: Option<f64>,
    #[serde(rename = "uah_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub uah24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vef: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vef_market_cap: Option<f64>,
    #[serde(rename = "vef_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vef24_h_vol: Option<f64>,
    #[serde(rename = "vef_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vef24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vnd: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vnd_market_cap: Option<f64>,
    #[serde(rename = "vnd_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vnd24_h_vol: Option<f64>,
    #[serde(rename = "vnd_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub vnd24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub zar: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub zar_market_cap: Option<f64>,
    #[serde(rename = "zar_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub zar24_h_vol: Option<f64>,
    #[serde(rename = "zar_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub zar24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xdr: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xdr_market_cap: Option<f64>,
    #[serde(rename = "xdr_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xdr24_h_vol: Option<f64>,
    #[serde(rename = "xdr_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xdr24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xag: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xag_market_cap: Option<f64>,
    #[serde(rename = "xag_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xag24_h_vol: Option<f64>,
    #[serde(rename = "xag_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xag24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xau: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xau_market_cap: Option<f64>,
    #[serde(rename = "xau_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xau24_h_vol: Option<f64>,
    #[serde(rename = "xau_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub xau24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bits: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bits_market_cap: Option<f64>,
    #[serde(rename = "bits_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bits24_h_vol: Option<f64>,
    #[serde(rename = "bits_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub bits24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sats: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sats_market_cap: Option<f64>,
    #[serde(rename = "sats_24h_vol")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sats24_h_vol: Option<f64>,
    #[serde(rename = "sats_24h_change")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(serialize_with = "serialize_float")]
    pub sats24_h_change: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_updated_at: Option<u64>,
}

impl From<coingecko::response::simple::Price> for Price {
    fn from(v: coingecko::response::simple::Price) -> Self {
        Self {
            btc: v.btc,
            btc_market_cap: v.btc_market_cap,
            btc24_h_vol: v.btc24_h_vol,
            btc24_h_change: v.btc24_h_change,
            eth: v.eth,
            eth_market_cap: v.eth_market_cap,
            eth24_h_vol: v.eth24_h_vol,
            eth24_h_change: v.eth24_h_change,
            ltc: v.ltc,
            ltc_market_cap: v.ltc_market_cap,
            ltc24_h_vol: v.ltc24_h_vol,
            ltc24_h_change: v.ltc24_h_change,
            bch: v.bch,
            bch_market_cap: v.bch_market_cap,
            bch24_h_vol: v.bch24_h_vol,
            bch24_h_change: v.bch24_h_change,
            bnb: v.bnb,
            bnb_market_cap: v.bnb_market_cap,
            bnb24_h_vol: v.bnb24_h_vol,
            bnb24_h_change: v.bnb24_h_change,
            eos: v.eos,
            eos_market_cap: v.eos_market_cap,
            eos24_h_vol: v.eos24_h_vol,
            eos24_h_change: v.eos24_h_change,
            xrp: v.xrp,
            xrp_market_cap: v.xrp_market_cap,
            xrp24_h_vol: v.xrp24_h_vol,
            xrp24_h_change: v.xrp24_h_change,
            xlm: v.xlm,
            xlm_market_cap: v.xlm_market_cap,
            xlm24_h_vol: v.xlm24_h_vol,
            xlm24_h_change: v.xlm24_h_change,
            link: v.link,
            link_market_cap: v.link_market_cap,
            link24_h_vol: v.link24_h_vol,
            link24_h_change: v.link24_h_change,
            dot: v.dot,
            dot_market_cap: v.dot_market_cap,
            dot24_h_vol: v.dot24_h_vol,
            dot24_h_change: v.dot24_h_change,
            yfi: v.yfi,
            yfi_market_cap: v.yfi_market_cap,
            yfi24_h_vol: v.yfi24_h_vol,
            yfi24_h_change: v.yfi24_h_change,
            usd: v.usd,
            usd_market_cap: v.usd_market_cap,
            usd24_h_vol: v.usd24_h_vol,
            usd24_h_change: v.usd24_h_change,
            aed: v.aed,
            aed_market_cap: v.aed_market_cap,
            aed24_h_vol: v.aed24_h_vol,
            aed24_h_change: v.aed24_h_change,
            ars: v.ars,
            ars_market_cap: v.ars_market_cap,
            ars24_h_vol: v.ars24_h_vol,
            ars24_h_change: v.ars24_h_change,
            aud: v.aud,
            aud_market_cap: v.aud_market_cap,
            aud24_h_vol: v.aud24_h_vol,
            aud24_h_change: v.aud24_h_change,
            bdt: v.bdt,
            bdt_market_cap: v.bdt_market_cap,
            bdt24_h_vol: v.bdt24_h_vol,
            bdt24_h_change: v.bdt24_h_change,
            bhd: v.bhd,
            bhd_market_cap: v.bhd_market_cap,
            bhd24_h_vol: v.bhd24_h_vol,
            bhd24_h_change: v.bhd24_h_change,
            bmd: v.bmd,
            bmd_market_cap: v.bmd_market_cap,
            bmd24_h_vol: v.bmd24_h_vol,
            bmd24_h_change: v.bmd24_h_change,
            brl: v.brl,
            brl_market_cap: v.brl_market_cap,
            brl24_h_vol: v.brl24_h_vol,
            brl24_h_change: v.brl24_h_change,
            cad: v.cad,
            cad_market_cap: v.cad_market_cap,
            cad24_h_vol: v.cad24_h_vol,
            cad24_h_change: v.cad24_h_change,
            chf: v.chf,
            chf_market_cap: v.chf_market_cap,
            chf24_h_vol: v.chf24_h_vol,
            chf24_h_change: v.chf24_h_change,
            clp: v.clp,
            clp_market_cap: v.clp_market_cap,
            clp24_h_vol: v.clp24_h_vol,
            clp24_h_change: v.clp24_h_change,
            cny: v.cny,
            cny_market_cap: v.cny_market_cap,
            cny24_h_vol: v.cny24_h_vol,
            cny24_h_change: v.cny24_h_change,
            czk: v.czk,
            czk_market_cap: v.czk_market_cap,
            czk24_h_vol: v.czk24_h_vol,
            czk24_h_change: v.czk24_h_change,
            dkk: v.dkk,
            dkk_market_cap: v.dkk_market_cap,
            dkk24_h_vol: v.dkk24_h_vol,
            dkk24_h_change: v.dkk24_h_change,
            eur: v.eur,
            eur_market_cap: v.eur_market_cap,
            eur24_h_vol: v.eur24_h_vol,
            eur24_h_change: v.eur24_h_change,
            gbp: v.gbp,
            gbp_market_cap: v.gbp_market_cap,
            gbp24_h_vol: v.gbp24_h_vol,
            gbp24_h_change: v.gbp24_h_change,
            hkd: v.hkd,
            hkd_market_cap: v.hkd_market_cap,
            hkd24_h_vol: v.hkd24_h_vol,
            hkd24_h_change: v.hkd24_h_change,
            huf: v.huf,
            huf_market_cap: v.huf_market_cap,
            huf24_h_vol: v.huf24_h_vol,
            huf24_h_change: v.huf24_h_change,
            idr: v.idr,
            idr_market_cap: v.idr_market_cap,
            idr24_h_vol: v.idr24_h_vol,
            idr24_h_change: v.idr24_h_change,
            ils: v.ils,
            ils_market_cap: v.ils_market_cap,
            ils24_h_vol: v.ils24_h_vol,
            ils24_h_change: v.ils24_h_change,
            inr: v.inr,
            inr_market_cap: v.inr_market_cap,
            inr24_h_vol: v.inr24_h_vol,
            inr24_h_change: v.inr24_h_change,
            jpy: v.jpy,
            jpy_market_cap: v.jpy_market_cap,
            jpy24_h_vol: v.jpy24_h_vol,
            jpy24_h_change: v.jpy24_h_change,
            krw: v.krw,
            krw_market_cap: v.krw_market_cap,
            krw24_h_vol: v.krw24_h_vol,
            krw24_h_change: v.krw24_h_change,
            kwd: v.kwd,
            kwd_market_cap: v.kwd_market_cap,
            kwd24_h_vol: v.kwd24_h_vol,
            kwd24_h_change: v.kwd24_h_change,
            lkr: v.lkr,
            lkr_market_cap: v.lkr_market_cap,
            lkr24_h_vol: v.lkr24_h_vol,
            lkr24_h_change: v.lkr24_h_change,
            mmk: v.mmk,
            mmk_market_cap: v.mmk_market_cap,
            mmk24_h_vol: v.mmk24_h_vol,
            mmk24_h_change: v.mmk24_h_change,
            mxn: v.mxn,
            mxn_market_cap: v.mxn_market_cap,
            mxn24_h_vol: v.mxn24_h_vol,
            mxn24_h_change: v.mxn24_h_change,
            myr: v.myr,
            myr_market_cap: v.myr_market_cap,
            myr24_h_vol: v.myr24_h_vol,
            myr24_h_change: v.myr24_h_change,
            ngn: v.ngn,
            ngn_market_cap: v.ngn_market_cap,
            ngn24_h_vol: v.ngn24_h_vol,
            ngn24_h_change: v.ngn24_h_change,
            nok: v.nok,
            nok_market_cap: v.nok_market_cap,
            nok24_h_vol: v.nok24_h_vol,
            nok24_h_change: v.nok24_h_change,
            nzd: v.nzd,
            nzd_market_cap: v.nzd_market_cap,
            nzd24_h_vol: v.nzd24_h_vol,
            nzd24_h_change: v.nzd24_h_change,
            php: v.php,
            php_market_cap: v.php_market_cap,
            php24_h_vol: v.php24_h_vol,
            php24_h_change: v.php24_h_change,
            pkr: v.pkr,
            pkr_market_cap: v.pkr_market_cap,
            pkr24_h_vol: v.pkr24_h_vol,
            pkr24_h_change: v.pkr24_h_change,
            pln: v.pln,
            pln_market_cap: v.pln_market_cap,
            pln24_h_vol: v.pln24_h_vol,
            pln24_h_change: v.pln24_h_change,
            rub: v.rub,
            rub_market_cap: v.rub_market_cap,
            rub24_h_vol: v.rub24_h_vol,
            rub24_h_change: v.rub24_h_change,
            sar: v.sar,
            sar_market_cap: v.sar_market_cap,
            sar24_h_vol: v.sar24_h_vol,
            sar24_h_change: v.sar24_h_change,
            sek: v.sek,
            sek_market_cap: v.sek_market_cap,
            sek24_h_vol: v.sek24_h_vol,
            sek24_h_change: v.sek24_h_change,
            sgd: v.sgd,
            sgd_market_cap: v.sgd_market_cap,
            sgd24_h_vol: v.sgd24_h_vol,
            sgd24_h_change: v.sgd24_h_change,
            thb: v.thb,
            thb_market_cap: v.thb_market_cap,
            thb24_h_vol: v.thb24_h_vol,
            thb24_h_change: v.thb24_h_change,
            try_field: v.try_field,
            try_market_cap: v.try_market_cap,
            try24_h_vol: v.try24_h_vol,
            try24_h_change: v.try24_h_change,
            twd: v.twd,
            twd_market_cap: v.twd_market_cap,
            twd24_h_vol: v.twd24_h_vol,
            twd24_h_change: v.twd24_h_change,
            uah: v.uah,
            uah_market_cap: v.uah_market_cap,
            uah24_h_vol: v.uah24_h_vol,
            uah24_h_change: v.uah24_h_change,
            vef: v.vef,
            vef_market_cap: v.vef_market_cap,
            vef24_h_vol: v.vef24_h_vol,
            vef24_h_change: v.vef24_h_change,
            vnd: v.vnd,
            vnd_market_cap: v.vnd_market_cap,
            vnd24_h_vol: v.vnd24_h_vol,
            vnd24_h_change: v.vnd24_h_change,
            zar: v.zar,
            zar_market_cap: v.zar_market_cap,
            zar24_h_vol: v.zar24_h_vol,
            zar24_h_change: v.zar24_h_change,
            xdr: v.xdr,
            xdr_market_cap: v.xdr_market_cap,
            xdr24_h_vol: v.xdr24_h_vol,
            xdr24_h_change: v.xdr24_h_change,
            xag: v.xag,
            xag_market_cap: v.xag_market_cap,
            xag24_h_vol: v.xag24_h_vol,
            xag24_h_change: v.xag24_h_change,
            xau: v.xau,
            xau_market_cap: v.xau_market_cap,
            xau24_h_vol: v.xau24_h_vol,
            xau24_h_change: v.xau24_h_change,
            bits: v.bits,
            bits_market_cap: v.bits_market_cap,
            bits24_h_vol: v.bits24_h_vol,
            bits24_h_change: v.bits24_h_change,
            sats: v.sats,
            sats_market_cap: v.sats_market_cap,
            sats24_h_vol: v.sats24_h_vol,
            sats24_h_change: v.sats24_h_change,
            last_updated_at: v.last_updated_at,
        }
    }
}

fn serialize_float<S>(v: &Option<f64>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match v {
        Some(v) => {
            if v < &1.0 {
                serializer.serialize_some(&format!("{:.4}", v))
            } else if v < &10.0 {
                serializer.serialize_some(&format!("{:.3}", v))
            } else if v < &1000.0 {
                serializer.serialize_some(&format!("{:.2}", v))
            } else {
                serializer.serialize_some(&(*v as u64).to_formatted_string(&Locale::fr))
            }
        }
        None => serializer.serialize_none(),
    }
}
