use std::collections::HashMap;

use clap::Parser;
use nasty_serde_hack::Price;
use pricer::Pricer;

mod nasty_serde_hack;
mod pricer;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    coins: Vec<String>,
    #[arg(short, long)]
    vs_curr: Vec<String>,
}

fn split_by_comma(strs: Vec<String>) -> Vec<String> {
    strs.into_iter()
        .flat_map(|v| {
            v.split(',')
                .map(|v| v.trim().to_string())
                .collect::<Vec<String>>()
        })
        .collect()
}

fn sort_by_key(
    referencial: Vec<String>,
    mut to_sort: HashMap<String, Price>,
) -> Vec<(String, Price)> {
    let mut result = Vec::new();

    for k in referencial {
        if let Some(v) = to_sort.remove(&k) {
            result.push((k, v));
        }
    }

    for (k, v) in to_sort.into_iter() {
        result.push((k, v));
    }

    result
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    let pricer = Pricer::new();

    let cw = split_by_comma(args.vs_curr);
    let coins = split_by_comma(args.coins);

    let prices = pricer.get_coin_stats(&coins, &cw).await?;

    let sorted_prices = sort_by_key(coins, prices);

    let sorted_prices_with_name_as_value = sorted_prices
        .into_iter()
        .map(|(k, v)| {
            serde_json::to_value(v).map(|mut v| {
                v["name"] = serde_json::Value::String(k);
                v
            })
        })
        .collect::<Result<Vec<_>, _>>()?;

    let result = serde_json::to_string(&sorted_prices_with_name_as_value)?;

    println!("{result}");

    Ok(())
}
