use std::collections::HashMap;

use anyhow::Context;
use coingecko::CoinGeckoClient;

use crate::nasty_serde_hack::Price;

pub struct Pricer {
    cg: CoinGeckoClient,
}

impl Pricer {
    pub fn new() -> Self {
        let cg = CoinGeckoClient::default();
        Self { cg }
    }

    pub async fn get_coin_stats(
        &self,
        coins: &[String],
        compare_with: &[String],
    ) -> anyhow::Result<HashMap<String, Price>> {
        let prices = self
            .cg
            .price(coins, compare_with, true, true, true, true)
            .await
            .with_context(|| "failed to fetch price")?.into_iter().map(|(k,v)| (k, v.into())).collect();

        Ok(prices)
    }
}
