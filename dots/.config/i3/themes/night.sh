#!/usr/bin/bash

# color temperature
redshift -x &
killall redshift
sleep 0.3
redshift -O 4000 &

# terminal theme
# rm $HOME/.config/alacritty/alacritty.yml
# cp --force $HOME/.config/alacritty/themes/alacritty_night.yml $HOME\/.config/alacritty/alacritty.yml


