#!/usr/bin/bash

# color temperature
# redshift -x &
# killall redshift
redshift -x &
killall redshift
sleep 0.3
redshift -O 6000 &

# terminal theme
# rm $HOME/.config/alacritty/alacritty.yml
# cp --force $HOME/.config/alacritty/themes/alacritty_day.yml $HOME/.config/alacritty/alacritty.yml


