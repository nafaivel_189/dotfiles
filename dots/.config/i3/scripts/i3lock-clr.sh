#/bin/bash
sleep 0.32
i3lock \
    --show-failed-attempts \
    --ignore-empty-password \
    \
    --blur=2 \
    --indicator \
    \
    --time-str="%H:%M:%S" \
    --date-str="%A, %m %Y" \
    --keylayout 0 \
    --date-color=#ffffffcc \
    --time-color=#ffffffcc \
    --layout-color=#ffffffcc \
    --wrongoutline-color=#ffffffcc \
    --verifoutline-color=#ffffffcc \
    \
    --verif-text="checking" \
    --noinput-text="nothing" \
    --wrong-text="nope" \
    \
    # --no-verify \
    # --{time, date, layout, verif, wrong, greeter}outline-color=
