#/bin/bash
source ~/.cache/wal/colors.sh

alpha='dd'
# exrorts from colors.sh
# background
# background='#282a36'
# selection='#44475a'
selection=$color0
comment='#6272a4'

# yellow='#f1fa8c'
# orange='#ffb86c'
# red='#ff5555'
# magenta='#ff79c6'
# blue='#6272a4'
# cyan='#8be9fd'
# green='50fa7b'

yellow=$color1
orange=$color2
red=$color3
magenta=$color4
blue=$color5
cyan=$color6
green=$color7

i3lock \
  --screen "$display_on" \
  --line-uses-inside \
  --clock --force-clock \
  --radius=10 \
  --ring-width=3 \
  \
  --ind-pos="x+260:y+h-40" \
  --time-pos="ix-215:iy-34" \
  --time-align 1 \
  --date-pos="ix-215:iy-14" \
  --date-align 1 \
  --greeter-pos="ix-265:iy+12" \
  --greeter-align 1 \
  --layout-pos="ix-215:iy+6" \
  --layout-align 1 \
  --verif-pos="ix-135:iy+6" \
  --verif-align 1 \
  --wrong-pos="ix-135:iy+6" \
  --wrong-align 1 \
  --modif-pos="ix+45:iy+43" \
  --modif-align 1 \
  \
  --pass-media-keys \
  --pass-screen-keys \
  --pass-volume-keys \
  --pass-power-keys \
  \
  --insidever-color=$selection$alpha \
  --insidewrong-color=$selection$alpha \
  --inside-color=$selection$alpha \
  --ringver-color=$green$alpha \
  --ringwrong-color=$red$alpha \
  --ringver-color=$green$alpha \
  --ringwrong-color=$red$alpha \
  --ring-color=$blue$alpha \
  --keyhl-color=$magenta$alpha \
  --bshl-color=$orange$alpha \
  --separator-color=$selection$alpha \
  --verif-color=$green \
  --wrong-color=$red \
  --layout-color=$blue \
  --date-color=$blue \
  --time-color=$blue \
  \
  --time-size=60 \
  --date-size=20 \
  --layout-size=14 \
  --verif-size=14 \
  --wrong-size=14 \
  --greeter-size=14 \
  \
  --screen 1 \
  --blur 1 \
  --clock \
  --indicator \
  --keylayout 0 \
  \
  --time-str="%H:%M:%S" \
  --date-str="%A %e %B %Y" \
  --verif-text="Checking..." \
  --wrong-text="Wrong pswd" \
  --noinput="" \
  --lock-text="Locking..." \
  --lockfailed="Lock Failed" \
  # --no-verify
#  --time-font="JetBrainsMono Nerd Font" \
#  --date-font="JetBrainsMono Nerd Font" \
#  --layout-font="JetBrainsMono Nerd Font" \
#  --verif-font="JetBrainsMono Nerd Font" \
#  --wrong-font="JetBrainsMono Nerd Font" \

# These last five lines are commented because they concern the font you want to use.
# JetBrainsMono Nerd Font is the font that was used in the screenshot.
# To specify a favorite font, just uncomment the five lines and replace the parameter with the font you prefer.
