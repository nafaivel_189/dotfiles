#!/bin/zsh
playerctl -a metadata --format '{{lc(status)}} {{playerName}}' | rg playing | awk '{print $2;}' | xargs playerctl play-pause -p
