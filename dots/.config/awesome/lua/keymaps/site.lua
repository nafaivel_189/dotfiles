local M = {}

local awful = require("awful")
local naughty = require("naughty")
local beautiful = require("beautiful")

M.resize_height = function(amount_persent)
  if client.focus then
    if client.focus.floating then
      client.focus.height = client.focus.height + (client.focus.height / 100 * amount_persent)
    else
      awful.client.incwfact(amount_persent / 100)
    end
  end
end

M.resize_width = function(amount_persent)
  if client.focus then
    if client.focus.floating then
      client.focus.width = client.focus.width + (client.focus.width / 100 * amount_persent)
    else
      awful.tag.incmwfact(amount_persent / 100)
    end
  end
end

M.toggle_notifications = function()
  if naughty.is_suspended() then
    naughty.toggle()
    naughty.notify({
      title = "Notifications",
      text = "on",
    })
  else
    naughty.notify({
      title = "Notifications",
      text = "off",
    })
    naughty.toggle()
  end
end

M.destroy_notifications = function()
  for s in screen do
    naughty.destroy_all_notifications({ screen = s })
  end
end

M.spawn_menu = function()
  require("lua.bar").mainmenu:toggle({
    coords = {
      x = screen.primary.geometry.width / 2 - beautiful.menu_width + beautiful.menu_width / 2,
      y = screen.primary.geometry.height / 2,
    },
  })
end

-- awmodoro git https://github.com/optama/awmodoro
local awmodoro = require("lua.awmodoro")

--pomodoro wibox
pomowibox = awful.wibox({ position = "top", screen = 1, height=1})
pomowibox.visible = false
M.pomodoro = awmodoro.new({
  minutes = 25,
  do_notify = true,
  active_bg_color = '#313131',
  paused_bg_color = '#7746D7',
  fg_color = {
     type = "linear",
     from = {0,0},
     to = {pomowibox.width, 0},
     stops = {
        {0, "#AECF96"},
        {0.5, "#88A175"},
        {1, "#FF5656"}
     }
  },
  width = pomowibox.width,
  height = pomowibox.height,

  begin_callback = function()
    -- for s = 1, screen.count() do
    -- 	mywibox[s].visible = false
    -- end
    pomowibox.visible = true
  end,

  finish_callback = function()
    -- for s = 1, screen.count() do
    -- 	mywibox[s].visible = true
    -- end

    awful.spawn.easy_async_with_shell("mpv ~/.config/awesome/audio/notify.mp3")
    pomowibox.visible = false
  end})
pomowibox:set_widget(M.pomodoro)

return M
