local M = {}

-- Standard awesome library
-- Theme handling library
local awful = require("awful")
local gears = require("gears")

function M.setup()
  local keys = {}
  local apps = require("lua.apps").setup()
  local site = require("lua.keymaps.site")
  local hotkeys_popup = require("awful.hotkeys_popup.widget")

  -- Default modkey.
  keys.modkey = "Mod4"
  local mod = keys.modkey
  local alt = "Mod1"
  local ctrl = "Control"
  local shift = "Shift"

  keys.globalkeys = gears.table.join(
    awful.key({	mod			}, "i", function () site.pomodoro:toggle() end),
    awful.key({	mod, "Shift"	}, "i", function () site.pomodoro:finish() end),
  -- Resize
    awful.key({ mod }, "Right", function()
      site.resize_width(5)
    end, { description = "increase master width factor", group = "layout" }),
    awful.key({ mod }, "Left", function()
      site.resize_width(-5)
    end, { description = "decrease master width factor", group = "layout" }),
    awful.key({ mod }, "Down", function()
      site.resize_height(5)
    end, { description = "increase master height factor", group = "layout" }),
    awful.key({ mod }, "Up", function()
      site.resize_height(-5)
    end, { description = "decrease master height factor", group = "layout" }),
    awful.key({ mod }, "l", function()
      site.resize_width(5)
    end, { description = "increase master width factor", group = "layout" }),
    awful.key({ mod }, "h", function()
      site.resize_width(-5)
    end, { description = "decrease master width factor", group = "layout" }),

    -- Navigation
    -- Tags
    awful.key({ mod }, "j", function()
      awful.client.focus.byidx(1)
    end, { description = "focus next by index", group = "client" }),
    awful.key({ mod }, "k", function()
      awful.client.focus.byidx(-1)
    end, { description = "focus previous by index", group = "client" }),
    awful.key({ mod }, "Tab", function()
      awful.client.focus.byidx(1)
    end, { description = "focus next by index", group = "client" }),
    awful.key({ mod, shift }, "Tab", function()
      awful.client.focus.byidx(-1)
    end, { description = "focus previous by index", group = "client" }),
    -- Screens
    awful.key({ mod, ctrl }, "j", function()
      awful.screen.focus_relative(1)
    end, { description = "focus the next screen", group = "screen" }),
    awful.key({ mod, ctrl }, "k", function()
      awful.screen.focus_relative(-1)
    end, { description = "focus the previous screen", group = "screen" }),
    -- Jump to urgent
    awful.key({ mod }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "screen" }),

    -- Layout manipulation
    awful.key({ mod, shift }, "j", function()
      awful.client.swap.byidx(1)
    end, { description = "swap with next client by index", group = "client" }),
    awful.key({ mod, shift }, "k", function()
      awful.client.swap.byidx(-1)
    end, { description = "swap with previous client by index", group = "client" }),
    awful.key({ mod, shift }, "l", function()
      awful.tag.incnmaster(1, nil, true)
    end, { description = "increase the number of master clients", group = "layout" }),
    awful.key({ mod, shift }, "h", function()
      awful.tag.incnmaster(-1, nil, true)
    end, { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ mod, ctrl }, "l", function()
      awful.tag.incncol(1, nil, true)
    end, { description = "increase the number of columns", group = "layout" }),
    awful.key({ mod, ctrl }, "h", function()
      awful.tag.incncol(-1, nil, true)
    end, { description = "decrease the number of columns", group = "layout" }),

    -- Launch applications keybindings
    -- Apps
    awful.key({ mod }, "Return", function()
      awful.spawn(apps.terminal)
    end, { description = "open a terminal", group = "launcher" }),
    awful.key({ mod, shift }, "e", function()
      awful.spawn(apps.editor_cmd)
    end, { description = "open a editor", group = "app" }),
    awful.key({ mod, shift }, "n", function()
      awful.spawn.with_shell(apps.note_taking)
    end, { description = "open a note taking app", group = "app" }),
    awful.key({ mod, ctrl, shift }, "n", function()
      awful.spawn.with_shell(apps.note_taking_alt)
    end, { description = "open a note taking app", group = "app" }),
    awful.key({ mod, shift }, "w", function()
      awful.spawn(apps.browser)
    end, { description = "open a web browser app", group = "app" }),
    awful.key({ mod, shift }, "f", function()
      awful.spawn(apps.filemanager_cli)
    end, { description = "open a cli filemanager", group = "filemanager" }),
    awful.key({ mod, shift, ctrl }, "f", function()
      awful.spawn(apps.filemanager)
    end, { description = "open a filemanager", group = "filemanager" }),
    awful.key({ mod, shift }, "u", function()
      awful.spawn.with_shell(apps.mixer)
    end, { description = "spawn pulsemixer", group = "app" }),
    -- Locker
    awful.key({ mod, alt }, "l", function()
      awful.spawn.with_shell("~/.config/i3/scripts/lock.sh")
    end, { description = "screen locker", group = "lock" }),

    -- Spawn awesome menu
    awful.key({ mod }, "w", function()
      site.spawn_menu()
    end, { description = "show main menu", group = "awesome" }),
    -- Rofi
    awful.key({ mod }, "d", function()
      awful.spawn("rofi -show drun")
    end, { description = "spawn launcher", group = "launcher" }),
    awful.key({ mod, shift }, "o", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/otd_mode.sh")
    end, { description = "spawn script to change active otd mode", group = "control" }),
    awful.key({ mod, shift }, "s", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/keys.sh")
    end, { description = "spawn script to copy symbol in clipboard", group = "control" }),
    awful.key({ mod, shift }, "r", function()
      awful.spawn.with_shell("~/.config/zathura/hstory_run.sh")
    end, { description = "spawn script to read books", group = "launcher" }),
    awful.key({ mod, shift }, "p", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/playerSwitch.py")
    end, { description = "spawn script to change active player", group = "control" }),
    awful.key({ mod, shift }, "d", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/night_day_mode.sh")
    end, { description = "change day_night mode", group = "control" }),
    awful.key({ mod, alt }, "n", function()
      awful.spawn.with_shell("networkmanager_dmenu")
    end, { description = "network settings", group = "control" }),
    awful.key({ mod, alt }, "b", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/dmenu-bluetooth")
    end, { description = "bluetooth settings", group = "control" }),
    awful.key({ mod, alt }, "m", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/rofi-beats")
    end, { description = "spawn rofi-beats", group = "music" }),
    awful.key({ mod, ctrl }, "c", function()
      awful.spawn.with_shell("~/.config/rofi/Scripts/click.sh")
    end, { description = "spawn autoclickers", group = "control" }),

    -- Volume Keys
    awful.key({}, "XF86AudioLowerVolume", function()
      awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ -3%", false)
    end),
    awful.key({}, "XF86AudioRaiseVolume", function()
      awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ +3%", false)
    end),
    awful.key({}, "XF86AudioMute", function()
      awful.util.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle", false)
    end),
    awful.key({}, "XF86AudioMicMute", function()
      awful.util.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle", false)
    end),

    -- Printscreen button
    awful.key({}, "Print", function()
      awful.util.spawn("flameshot gui", false)
    end),

    -- Media Keys
    awful.key({}, "Pause", function()
      awful.util.spawn("playerctl -i kdeconnect,mpd play-pause", false)
    end),
    awful.key({}, "XF86AudioPlay", function()
      awful.util.spawn("playerctl -i kdeconnect,mpd play-pause", false)
    end),
    awful.key({}, "XF86AudioNext", function()
      awful.util.spawn("playerctl -i mpd,kdeconnect next", false)
    end),
    awful.key({}, "XF86AudioPrev", function()
      awful.util.spawn("playerctl -i mpd,kdeconnect previous", false)
    end),

    -- MPD
    awful.key({ mod }, "Pause", function()
      awful.util.spawn("playerctl -p mpd play-pause", false)
    end, { description = "mpd: play-pause", group = "music" }),
    awful.key({ mod }, "XF86AudioPlay", function()
      awful.util.spawn("playerctl -p mpd play-pause", false)
    end, { description = "mpd: play-pause", group = "music" }),
    awful.key({ mod }, "XF86AudioNext", function()
      awful.util.spawn("playerctl -p mpd next", false)
    end, { description = "mpd: next", group = "music" }),
    awful.key({ mod }, "XF86AudioPrev", function()
      awful.util.spawn("playerctl -p mpd previous", false)
    end, { description = "mpd: prev", group = "music" }),
    awful.key({ mod }, "XF86AudioRaiseVolume", function()
      awful.util.spawn("playerctl -p mpd volume 0.05+", false)
    end, { description = "mpd: vol up", group = "music" }),
    awful.key({ mod }, "XF86AudioLowerVolume", function()
      awful.util.spawn("playerctl -p mpd volume 0.05-", false)
    end, { description = "mpd: vol down", group = "music" }),

    -- kdeconnect
    awful.key({ ctrl }, "Pause", function()
      awful.util.spawn("playerctl -p kdeconnect play-pause", false)
    end, { description = "kdeconn: play-pause", group = "music" }),
    awful.key({ ctrl }, "XF86AudioPlay", function()
      awful.util.spawn("playerctl -p kdeconnect play-pause", false)
    end, { description = "kdeconn: play-pause", group = "music" }),
    awful.key({ ctrl }, "XF86AudioNext", function()
      awful.util.spawn("playerctl -p kdeconnect next", false)
    end, { description = "kdeconn: next", group = "music" }),
    awful.key({ ctrl }, "XF86AudioPrev", function()
      awful.util.spawn("playerctl -p kdeconnect previous", false)
    end, { description = "kdeconn: prev", group = "music" }),
    awful.key({ ctrl }, "XF86AudioRaiseVolume", function()
      awful.util.spawn("playerctl -p kdeconnect volume 0.05+", false)
    end, { description = "kdeconn: vol up", group = "music" }),
    awful.key({ ctrl }, "XF86AudioLowerVolume", function()
      awful.util.spawn("playerctl -p kdeconnect volume 0.05-", false)
    end, { description = "kdeconn: vol down", group = "music" }),

    -- Brightness keys
    awful.key({}, "XF86MonBrightnessUp", function()
      awful.util.spawn("brightnessctl --device=amdgpu_bl0 s 5+", false)
    end),
    awful.key({}, "XF86MonBrightnessDown", function()
      awful.util.spawn("brightnessctl --device=amdgpu_bl0 s 5-", false)
    end),
    awful.key({mod, ctrl}, "bracketright", function()
      awful.util.spawn("brightnessctl --device=amdgpu_bl0 s 5+", false)
    end, { description = "+", group = "brightness" }),
    awful.key({mod, ctrl}, "bracketleft", function()
      awful.util.spawn("brightnessctl --device=amdgpu_bl0 s 5-", false)
    end, { description = "-", group = "brightness" }),

    -- Notifications control
    awful.key({ mod, ctrl }, "grave", function()
      site.toggle_notifications()
    end, { description = "toggle notifications", group = "notifications" }),
    awful.key({ mod, ctrl }, "Escape", function()
      site.destroy_notifications()
    end, { description = "clear notifications", group = "notifications" }),

    -- Awesome controls
    awful.key({ mod, ctrl }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }),
    awful.key({ mod }, "space", function()
      awful.layout.inc(1)
    end, { description = "select next", group = "layout" }),
    awful.key({ mod, shift }, "space", function()
      awful.layout.inc(-1)
    end, { description = "select previous", group = "layout" }),

    -- hotkeys popup help
    awful.key({ mod }, "/", function()
      hotkeys_popup.show_help()
    end, nil)
  )

  keys.clientkeys = gears.table.join(
  -- State
  -- Fullscreen
    awful.key({ mod }, "f", function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end, { description = "toggle fullscreen", group = "client" }),
    -- Floating
    awful.key(
      { mod, ctrl },
      "space",
      awful.client.floating.toggle,
      { description = "toggle floating", group = "client" }
    ),
    -- On top
    awful.key({ mod }, "t", function(c)
      c.ontop = not c.ontop
    end, { description = "toggle keep on top", group = "client" }),
    -- On top
    awful.key({ mod }, "s", function(c)
      c.sticky = not c.sticky
    end, { description = "toggle sticky", group = "client" }),
    -- Maximize
    awful.key({ mod }, "m", function(c)
      c.maximized = not c.maximized
      c:raise()
    end, { description = "(un)maximize", group = "client" }),
    awful.key({ mod, ctrl }, "m", function(c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end, { description = "(un)maximize vertically", group = "client" }),
    awful.key({ mod, shift }, "m", function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end, { description = "(un)maximize horizontally", group = "client" }),
    -- Hide unhide
    awful.key({ mod }, "n", function(c)
      c.minimized = true
    end, { description = "minimize", group = "client" }),
    awful.key({ mod, ctrl }, "n", function()
      local c = awful.client.restore()
      if c then
        c:emit_signal("request::activate", "key.unminimize", { raise = true })
      end
    end, { description = "restore minimized", group = "client" }),

    -- Actions
    awful.key({ mod, shift }, "q", function(c)
      c:kill()
    end, { description = "close", group = "client" }),

    -- Position
    awful.key({ mod, ctrl }, "Return", function(c)
      c:swap(awful.client.getmaster())
    end, { description = "move to master", group = "client" }),
    -- Move window to monitors
    awful.key({ mod }, "o", function(c)
      c:move_to_screen()
    end, { description = "move to next screen", group = "client" }),
    awful.key({ mod, ctrl, shift }, "k", function(c)
      c:move_to_screen(c.screen.index - 1)
    end, { description = "move to screen left", group = "client" }),
    awful.key({ mod, ctrl, shift }, "j", function(c)
      c:move_to_screen(c.screen.index + 1)
    end, { description = "move to screen right", group = "client" })
  )

  -- Bind all key numbers to tags.
  -- Be careful: we use keycodes to make it work on any keyboard layout.
  -- This should map on the top row of your keyboard, usually 1 to 9.
  for i = 1, 9 do
    keys.globalkeys = gears.table.join(
      keys.globalkeys,
      -- View tag only.
      awful.key({ mod }, "#" .. i + 9, function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end),
      -- Toggle tag display.
      awful.key({ mod, ctrl }, "#" .. i + 9, function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end),
      -- Move client to tag.
      awful.key({ mod, shift }, "#" .. i + 9, function()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end),
      -- Toggle tag on focused client.
      awful.key({ mod, ctrl, shift }, "#" .. i + 9, function()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end)
    )
  end

  keys.clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ mod }, 1, function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.move(c)
    end),
    awful.button({ mod }, 3, function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.resize(c)
    end),
    awful.button({ mod }, 4, function(c)
      awful.client.focus.byidx(-1)
    end),
    awful.button({ mod }, 5, function(c)
      awful.client.focus.byidx(1)
    end),
    awful.button({ mod, ctrl }, 4, function(c)
      awful.tag.viewprev(c.screen)
    end),
    awful.button({ mod, ctrl }, 5, function(c)
      awful.tag.viewnext(c.screen)
    end)
  )

  -- Set keys
  root.keys(keys.globalkeys)

  return keys
end

return M
