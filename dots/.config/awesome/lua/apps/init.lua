M = {}

function M.setup()
	local terminal = "alacritty"
	--[[ local browser = "librewolf" ]]
	local browser = "qutebrowser"
	local editor = os.getenv("EDITOR") or "nvim"

	local function get_cli_with_wal_name(prog)
		local command = terminal
			.. " -e zsh -c \"(cat ~/.cache/wal/sequences && printf %b '\\e]11;#000000\a' &) && "
			.. prog
			.. '"'
		return command
	end

	local editor_cmd = --[[ terminal .. " -e " .. ]] editor .. " -c"
	local filemanager = "thunar"
	-- local filemanager_cli = terminal .. " -e ranger"
	local filemanager_cli = get_cli_with_wal_name("lf")
	local note_taking = "~/.config/rofi/Scripts/new_note.sh"
	local note_taking_alt = get_cli_with_wal_name("kastler")
  local mixer = get_cli_with_wal_name("pulsemixer")

	return {
		browser = browser,
		terminal = terminal,
		editor = editor,
		editor_cmd = editor_cmd,
		filemanager = filemanager,
		filemanager_cli = filemanager_cli,
		note_taking = note_taking,
		note_taking_alt = note_taking_alt,
    mixer = mixer,
	}
end

return M
