local awful = require("awful")
local naughty = require("naughty")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gfs = require("gears.filesystem")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- dev

local network_widget = {}

local function worker(user_args)
  local args = user_args or {}

  local WIFI_COMMAND = [[iw dev %s link]]
  local WIFI_NAME_COMMAND = [[iw dev %s link | grep "SSID: " | sed 's/SSID: //' | sed 's/^[ \t]*//']]

  local path_to_icons = args.path_to_icons or "/usr/share/icons/Papirus-Dark/symbolic/status/"
  --[[ local path_to_icons = args.path_to_icons or "/usr/share/icons/Papirus-Light/symbolic/status/" ]]
  local margin_left = args.margin_left or 0
  local margin_right = args.margin_right or 0

  local display_notification = args.display_notification or false
  local display_notification_onClick = args.display_notification_onClick or true
  local timeout = args.timeout or 10
  local network_menu_command = args.network_menu_command or nil
  local network_menu_command_alt = args.network_menu_command_alt or nil
  local show_wifi_name = args.show_wifi_name or false
  local font = args.font or beautiful.font
  local devices = args.devices or { "wlan0", "eno1" }

  if not gfs.dir_readable(path_to_icons) then
    naughty.notify({
      title = "Network Widget",
      text = "Folder with icons doesn't exist: " .. path_to_icons,
      preset = naughty.config.presets.critical,
    })
  end

  local function trim_end(stdout)
    stdout = stdout:match("^(.*)\n")
    return stdout
  end

  local nw_widget = wibox.widget({
    {
      id = "icon",
      widget = wibox.widget.imagebox,
      resize = false,
    },
    valign = "center",
    layout = wibox.container.place,
  })
  network_widget = wibox.widget({
    nw_widget,
    {
      id = "text",
      font = font,
      widget = wibox.widget.textbox,
    },
    layout = wibox.layout.fixed.horizontal,
  })

  -- Popup with bluetooth info
  -- One way of creating a pop-up notification - naughty.notify
  local notification
  local function show_status(networkType)
    awful.spawn.easy_async_with_shell(string.format(WIFI_NAME_COMMAND, devices[1]), function(stdout)
      naughty.destroy(notification)
      notification = naughty.notify({
        text = trim_end(stdout),
        title = "Connection",
        icon = path_to_icons .. networkType .. ".svg",
        icon_size = dpi(16),
        timeout = 5,
        hover_timeout = 0.5,
        width = 200,
        screen = mouse.screen,
      })
    end)
  end

  local networkType = "network-wireless-disconnected-symbolic"

  watch("bash -c '" .. string.format(WIFI_COMMAND, devices[1]) .. "'", timeout, function(widget, stdout)
    local wifi_info = {}
    wifi_info.status = trim_end(stdout)
    local status = wifi_info.status
    if string.find(status , "Not connected.") then
      networkType = "network-wireless-disconnected-symbolic"
    else
      networkType = "network-wireless-signal-excellent-symbolic"
    end
    widget.icon:set_image(path_to_icons .. networkType .. ".svg")
    widget:get_children_by_id("text")[1]:set_text("dsa")


    -- Update popup text
    -- battery_popup.text = string.gsub(stdout, "\n$", "")
  end, nw_widget)

  if display_notification then
    network_widget:connect_signal("mouse::enter", function()
      show_status(networkType)
    end)
    network_widget:connect_signal("mouse::leave", function()
      naughty.destroy(notification)
    end)
  elseif display_notification_onClick then
    network_widget:connect_signal("button::press", function(_, _, _, button)
      if button == 1 then
        show_status(networkType)
      end
      if button == 2 then
        if network_menu_command_alt then
          awful.spawn.with_shell(network_menu_command_alt)
        end
      end
      if button == 3 then
        if network_menu_command then
          awful.spawn.with_shell(network_menu_command)
        end
      end
    end)
    network_widget:connect_signal("mouse::leave", function()
      naughty.destroy(notification)
    end)
  end

  return wibox.container.margin(network_widget, margin_left, margin_right)
end

return setmetatable(network_widget, {
  __call = function(_, ...)
    return worker(...)
  end,
})
