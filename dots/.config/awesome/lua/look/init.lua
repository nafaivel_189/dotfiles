local M = {}

local beautiful = require("beautiful")
local gears = require("gears")
local awful = require("awful")

function M.set_wallpaper(s)
	-- Wallpaper
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s)
	end
end

function M.setup()
	--[[ local theme_path = string.format("%s/.config/awesome/themes/%s/%s/theme.lua", os.getenv("HOME"), "dark", "Ix3doEQL") ]]
  local path = "~/.config/awesome/themes/dark/5wwwq3/"
  local theme_path = path:gsub("~", os.getenv("HOME")) .. "theme.lua"
	beautiful.init(theme_path)
	beautiful.menu_height = 40
	beautiful.menu_width = 180
	for s = 1, screen.count() do
		gears.wallpaper.maximized(beautiful.wallpaper, s, true)
	end

	-- for change term color and thing like that
	--[[ awful.spawn.once("wal -i " .. beautiful.wallpaper) ]]
	return M
end

-- No borders if only one window on screen
function M.border_adjust(c)
	if #c.screen.clients == 1 then
		c.border_width = 0
	elseif (mouse.screen.selected_tag.layout.name == "max") or (mouse.screen.selected_tag.layout.name == "fullscreen") then
		c.border_width = 0
	elseif #c.screen.clients > 1 then
		c.border_width = beautiful.border_width
		c.border_color = beautiful.border_focus
	end
end

return M
