-- Source *.lua cfgs
require("lua.autostart")

local look = require("lua.look").setup()
local keys = require("lua.keymaps").setup()
local apps = require("lua.apps").setup()
local bar = require("lua.bar").setup()

-- local ok, widgets = pcall(require,"lua.widgets")
-- if ok then
--   --[[ widgets.setup() ]]
-- else
-- end

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
-- require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors,
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function(err)
    -- Make sure we don't go into an endless error loop
    if in_error then
      return
    end
    in_error = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Error",
      text = tostring(err),
    })
    in_error = false
  end)
end
-- }}}

-- {{{ Variable definitions
-- Table of layouts to cover with awful.layout.inc, order matters.
local l = awful.layout.suit -- Just to save some typing: use an alias.
awful.layout.layouts = {
  l.tile.right,
  -- l.tile.left,
  l.max,
  l.max.fullscreen,
  l.fullscreen,
}

-- Tags
local names = {
  "sec", -- 1
  "main", -- 2
  "doc", -- 3
  "bg",  -- 4
  "msg", -- 5
  "fm",  -- 6
  "3w",  -- 7
  "vid", -- 8
  "m/g", -- 9
}
local l = awful.layout.suit
local layouts = {
  l.tile.right, -- 1
  l.tile.right, -- 2
  l.tile.right, -- 3
  l.tile.right, -- 4
  l.tile.right, -- 5
  l.tile.right, -- 6
  l.tile.right, -- 7
  l.tile.right, -- 8
  l.tile.right, -- 9
}
awful.tag(names, s, layouts)

menubar.utils.terminal = apps.terminal -- Set the terminal for applications that require it

-- {{{ Mouse bindings
root.buttons(gears.table.join(
  awful.button({}, 3, function()
    bar.mainmenu:toggle()
  end),
  awful.button({}, 1, function()
    bar.mainmenu:hide()
  end)
-- awful.button({}, 4, awful.tag.viewnext),
-- awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
  -- All clients will match this rule.
  {
    rule = {},
    properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = awful.client.focus.filter,
      --[[ floating = false, ]]
      --[[ raise = true, ]]
      keys = keys.clientkeys,
      buttons = keys.clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
    },
  },

  -- Floating clients.
  {
    rule_any = {
      instance = {
        "DTA", -- Firefox addon DownThemAll.
        "copyq", -- Includes session name in class.
        "pinentry",
      },
      class = {
        "Arandr",
        "Blueman-manager",
        "Gpick",
        "Kruler",
        "MessageWin", -- kalarm.
        "Sxiv",
        "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
        "System-monitoring-center",
        "Wpa_gui",
        "veromix",
        "xtightvncviewer",
        "steam.exe",
      },
      -- Note that the name property shown in xprop might be set slightly after creation of the client
      -- and the name shown there might not match defined rules here.
      name = {
        "Event Tester", -- xev.
      },
      role = {
        "AlarmWindow", -- Thunderbird's calendar.
        "ConfigManager", -- Thunderbird's about:config.
        "pop-up",    -- e.g. Google Chrome's (detached) Developer Tools.
      },
      properties = {
        floating = true,
      },
    },
    { rule = { instance = "war3.exe" }, properties = { floating = true } },
  },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
  c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

-- client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("focus", look.border_adjust)
client.connect_signal("unfocus", function(c)
  c.border_color = beautiful.border_normal
end)
-- }}}
