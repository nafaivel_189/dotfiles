#!/usr/bin/env bash

# Terminate already running bar instances
killall -q waybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
# polybar mybar 2>&1 | tee -a /tmp/polybar1.log & disown
waybar > ~/.config/waybar/waybar.log & disown
echo "Bars launched..."
