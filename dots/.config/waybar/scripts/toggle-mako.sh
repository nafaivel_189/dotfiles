#!/bin/bash

if [[ "$1" != "listen" ]]; then
  makoctl mode -t hide
  exit 0
fi

# Toggle DND
if [ "$(makoctl mode | tail -n1)" == "hide" ]; then
  # DND ON
  echo ""
else
  # DND OFF
  echo ""
fi

