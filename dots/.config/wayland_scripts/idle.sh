#!/bin/bash
killall swayidle
swayidle -w \
    timeout 600 '~/.config/wayland_scripts/lock.sh' \
    before-sleep 'playerctl pause' \
    before-sleep '~/.config/wayland_scripts/lock.sh' &
    # NOTE: if you want to use screen fading and suspend paste this near other timeout
    # timeout 300 'for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --toggle; done;' \
    #     resume 'for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --toggle; done;' \
    # timeout 1800 'systemctl suspend' \
