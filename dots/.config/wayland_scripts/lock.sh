#/bin/bash

source ~/.cache/wal/colors.sh

alpha='dd'
# exrorts from colors.sh

IMG="/home/antasa/Pictures/walls/wallhaven-x128m3.png"
IMG=$(rg -o "swaybg -o '[^']*' -i \"[^']*\"" ~/.azotebg | sed "s/.*-i \"\([^']*\)\".*$/\1/" | head -n1)

yellow=$color1
orange=$color2
red=$color3
magenta=$color4
blue=$color5
cyan=$color6
green=$color7

if pgrep -x "swaylock" > /dev/null
then
  return
fi

swaylock \
    --daemonize \
    --show-failed-attempts \
    --ignore-empty-password \
    --show-keyboard-layout \
    --indicator-caps-lock \
    --image $IMG \
    --indicator-radius 100 \
    --color=282a36 \
    --inside-color=$background \
    --line-color=$background \
    --ring-color=$magenta \
    --text-color=$foreground \
    --layout-bg-color=$background \
    --layout-text-color=$foreground \
    --inside-clear-color=$yellow \
    --line-clear-color=$background \
    --ring-clear-color=$yellow \
    --text-clear-color=$background \
    --inside-ver-color=$magenta \
    --line-ver-color=$background \
    --ring-ver-color=$magenta \
    --text-ver-color=$background \
    --inside-wrong-color=$red \
    --line-wrong-color=$background \
    --ring-wrong-color=$red \
    --text-wrong-color=$background \
    --bs-hl-color=$red \
    --key-hl-color=$green \
    --text-caps-lock-color=$foreground \
