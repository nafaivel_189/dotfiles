#!/bin/bash

text=$(wl-paste)
corrected_text=$(hunspell -d "en_US,by_BE" -a <(echo "$text") | sed '1d')

# NOTE: Trim clip len to 37 chars to prevent linebreak in mako for my setup. Feel free to update
notify-send -u low "${text:0:37}" "$corrected_text"
