#!/bin/bash

path="$HOME/Pictures/Screenshots/$(date +%Y-%m-%d_%H-%M-%S).png"
grim -g "$(slurp)" "$path" && notify-send "screenshot saved" "$path" -u low
