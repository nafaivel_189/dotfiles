#!/usr/bin/bash

declare -a options=(
"night"
"day"
)

choise=$(printf '%s\n' "${options[@]}" | wofi --dmenu -i -l 3 -p "choose mode:")

if [ "$choise" = night ]; then
  $HOME/.config/wofi/scripts/ndm/night.sh
elif [ "$choise" = day ]; then
  exec $HOME/.config/wofi/scripts/ndm/day.sh
else
    echo "OK!" && exit 1
fi

