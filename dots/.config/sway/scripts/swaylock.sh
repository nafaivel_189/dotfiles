#/bin/bash
IMG="/home/antasa/Pictures/walls/wallhaven-x128m3.png"

swaylock \
    -F \
    -e \
    -k \
    -l \
    --image $IMG \
    --indicator-radius 100 \
    --ring-color bcd4e6 \
    --key-hl-color ffbcd4e6 \
    --line-color 00000000 \
    --separator-color 00000000 \
