#/bin/zsh
ps -aux | rg -e OpenTabletDriver.Daemon.dll | rg -v rg | awk  '{print $2}' | xargs kill
exec dotnet /usr/share/OpenTabletDriver/OpenTabletDriver.Daemon.dll
