# vars
#  _   ______ ___________
# | | / / __ `/ ___/ ___/
# | |/ / /_/ / /  (__  )
# |___/\__,_/_/  /____/
$terminal = foot
$editor = $terminal -e nvim 
$editor_alt = emacsclient -c 
$notes = ~/.config/rofi/Scripts/new_note.sh
$notes_alt = $terminal -e ~/.config/help_scripts/fix_colors_for.sh kastler
$browser = qutebrowser
$browser_alt = librewolf
$fm = $terminal -e ~/.config/help_scripts/fix_colors_for.sh lf
$fm_alt = thunar
$mixer = $terminal -e ~/.config/help_scripts/fix_colors_for.sh pulsemixer
$locker = ~/.config/wayland_scripts/lock.sh
$video_device = amdgpu_bl0
$MOD = SUPER


# apps
#   ____ _____  ____  _____
#  / __ `/ __ \/ __ \/ ___/
# / /_/ / /_/ / /_/ (__  )
# \__,_/ .___/ .___/____/
#     /_/   /_/
bind = $MOD,          Return, exec, $terminal
bind = $MOD Shift,         e, exec, $editor
bind = $MOD Shift Control, e, exec, $editor_alt
bind = $MOD Shift,         n, exec, $notes
bind = $MOD Shift Control, n, exec, $notes_alt
bind = $MOD Shift,         w, exec, $browser
bind = $MOD Shift Control, w, exec, $browser_alt
bind = $MOD Shift,         f, exec, $fm
bind = $MOD Shift Control, f, exec, $fm_alt
bind = $MOD Shift,         u, exec, $mixer
bind = $MOD Shift,         t, exec, ~/.config/wayland_scripts/translate-clip.sh # translate text form clipboard and show in notification
bind = $MOD,               b, exec, $locker
# wofi
bind = $MOD,       d, exec, wofi --show drun --allow-images --insensetive --no-actions
bind = $MOD Shift, c, exec, ~/.config/wofi/scripts/wofi-calc
bind = $MOD Shift, o, exec, ~/.config/rofi/Scripts/otd_mode.sh
bind = $MOD Shift, s, exec, ~/.config/rofi/Scripts/keys.sh
bind = $MOD Shift, r, exec, ~/.config/zathura/hstory_run.sh
bind = $MOD Shift, p, exec, ~/.config/rofi/Scripts/playerSwitch.py
bind = $MOD Shift, d, exec, ~/.config/rofi/Scripts/night_day_mode.sh
bind = $MOD Alt,   n, exec, networkmanager_dmenu
bind = $MOD Alt,   b, exec, ~/.config/rofi/Scripts/dmenu-bluetooth
bind = $MOD Alt,   m, exec, ~/.config/rofi/Scripts/rofi-beats


# binds keys
#     __    _           __
#    / /_  (_)___  ____/ /____
#   / __ \/ / __ \/ __  / ___/
#  / /_/ / / / / / /_/ (__  )
# /_.___/_/_/ /_/\__,_/____/

# Control pulse audio volume with pactl
binde = , XF86AudioRaiseVolume,         exec, pactl set-sink-volume @DEFAULT_SINK@ +3%
binde = , XF86AudioLowerVolume,         exec, pactl set-sink-volume @DEFAULT_SINK@ -3%
bind = , XF86AudioMute,                exec, pactl set-sink-mute @DEFAULT_SINK@ toggle

# Control pulse audio volume with pactl
binde = $MOD, XF86AudioRaiseVolume,     exec, playerctl -p mpd volume 0.05+
binde = $MOD, XF86AudioLowerVolume,     exec, playerctl -p mpd volume 0.05-

# Control pulse audio volume with pactl
binde = Control, XF86AudioRaiseVolume,  exec, playerctl -p kdeconnect volume 0.05+
binde = Control, XF86AudioLowerVolume,  exec, playerctl -p kdeconnect volume 0.05-

# Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
bind = ,XF86AudioMedia,                exec, playerctl -i mpd,kdeconnect play-pause
bind = ,XF86AudioPlay ,                exec, playerctl -i mpd,kdeconnect play-pause
bind = ,Pause         ,                exec, playerctl -i mpd,kdeconnect play-pause
bind = ,XF86AudioPrev ,                exec, playerctl -i mpd,kdeconnect previous
bind = ,XF86AudioNext ,                exec, playerctl -i mpd,kdeconnect next

# Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
bind = $MOD, XF86AudioMedia,           exec, playerctl -p mpd play-pause
bind = $MOD, XF86AudioPlay ,           exec, playerctl -p mpd play-pause
bind = $MOD, Pause         ,           exec, playerctl -p mpd play-pause
bind = $MOD, XF86AudioPrev ,           exec, playerctl -p mpd previous
bind = $MOD, XF86AudioNext ,           exec, playerctl -p mpd next

# Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
bind = Control, XF86AudioMedia,        exec, playerctl -p kdeconnect play-pause
bind = Control, XF86AudioPlay,         exec, playerctl -p kdeconnect play-pause
bind = Control, Pause,                 exec, playerctl -p kdeconnect play-pause
bind = Control, XF86AudioPrev,         exec, playerctl -p kdeconnect previous
bind = Control, XF86AudioNext,         exec, playerctl -p kdeconnect next

# Control screen backlight brightness with light (https://github.com/haikarainen/light)
binde = , XF86MonBrightnessUp,          exec, brightnessctl -d $video_device s 5+
binde = , XF86MonBrightnessDown,        exec, brightnessctl -d $video_device s 5-
binde = $MOD Control, bracketright,     exec, brightnessctl -d $video_device s 5+
binde = $MOD Control, bracketleft,      exec, brightnessctl -d $video_device s 5-

# screenshots
bind =    ,        Print, exec, ~/.config/wayland_scripts/screenshots/partclip.sh
bind = Alt,        Print, exec, ~/.config/wayland_scripts/screenshots/partsave.sh
bind = Control,    Print, exec, ~/.config/wayland_scripts/screenshots/fullsave.sh

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $MOD Shift,      q, killactive, 
bind = $MOD Alt,        q, exit,
bind = $MOD Alt,        r, exec, hyprctl reload && notify-send -u low "hypr" "config rereaded"
bind = $MOD,        Space, togglefloating,
bind = $MOD,            f, fullscreen,
bind = $MOD,            m, exec, hyprctl keyword general:layout "dwindle" 
bind = $MOD,            t, exec, hyprctl keyword general:layout "master" 

# Move focus
bind = $MOD, j, cyclenext
bind = $MOD, k, cyclenext, prev
binde = $MOD, h, splitratio, -0.1
binde = $MOD, l, splitratio, +0.1
# bind = $MOD, h, resizeactive, -100 0
# bind = $MOD, l, resizeactive, 100 0
# bind = $MOD, h, movefocus, l
# bind = $MOD, j, movefocus, d
# bind = $MOD, k, movefocus, u
# bind = $MOD, l, movefocus, r

# Swap windows
bind = $MOD Shift, h, layoutmsg, removemaster
bind = $MOD Shift, j, swapnext
bind = $MOD Shift, k, swapnext, prev
bind = $MOD Shift, l, layoutmsg, addmaster

# Move windows
bind = $MOD Control, h, movewindow, l
bind = $MOD Control, j, movewindow, d
bind = $MOD Control, k, movewindow, u
bind = $MOD Control, l, movewindow, r

# Resize windows
binde = $MOD Alt,   j, resizeactive, 0 20
binde = $MOD Alt,   k, resizeactive, 0 -20
binde = $MOD Alt,   l, resizeactive, 20 0
binde = $MOD Alt,   h, resizeactive, -20 0

# Resize windows
binde = $MOD Shift Alt,   j, moveactive, 0 20
binde = $MOD Shift Alt,   k, moveactive, 0 -20
binde = $MOD Shift Alt,   l, moveactive, 20 0
binde = $MOD Shift Alt,   h, moveactive, -20 0


# workspaces wscs
#                       __                                  
#  _      ______  _____/ /___________  ____ _________  _____
# | | /| / / __ \/ ___/ //_/ ___/ __ \/ __ `/ ___/ _ \/ ___/
# | |/ |/ / /_/ / /  / ,< (__  ) /_/ / /_/ / /__/  __(__  ) 
# |__/|__/\____/_/  /_/|_/____/ .___/\__,_/\___/\___/____/  
#                            /_/                            

general {
  layout = master
}

master {
  mfact = 0.6
  orientation = left
  new_status = master
}

# Switch workspaces with MOD + [0-9]
bind = $MOD, 1, workspace, 1
bind = $MOD, 2, workspace, 2
bind = $MOD, 3, workspace, 3
bind = $MOD, 4, workspace, 4
bind = $MOD, 5, workspace, 5
bind = $MOD, 6, workspace, 6
bind = $MOD, 7, workspace, 7
bind = $MOD, 8, workspace, 8
bind = $MOD, 9, workspace, 9
bind = $MOD, 0, workspace, 10

# Move active window to a workspace with MOD + SHIFT + [0-9]
bind = $MOD SHIFT, 1, movetoworkspace, 1
bind = $MOD SHIFT, 2, movetoworkspace, 2
bind = $MOD SHIFT, 3, movetoworkspace, 3
bind = $MOD SHIFT, 4, movetoworkspace, 4
bind = $MOD SHIFT, 5, movetoworkspace, 5
bind = $MOD SHIFT, 6, movetoworkspace, 6
bind = $MOD SHIFT, 7, movetoworkspace, 7
bind = $MOD SHIFT, 8, movetoworkspace, 8
bind = $MOD SHIFT, 9, movetoworkspace, 9
bind = $MOD SHIFT, 0, movetoworkspace, 10


# devices
#        __          _               
#   ____/ /__ _   __(_)_______  _____
#  / __  / _ \ | / / / ___/ _ \/ ___/
# / /_/ /  __/ |/ / / /__/  __(__  ) 
# \____/\___/|___/_/\___/\___/____/  
input {
  # layouts
  kb_layout = us,by
  kb_options = grp:caps_toggle,compose:ins,compose:menu 

  # kb
  repeat_rate = 40
  repeat_delay = 200

  # mouse
  follow_mouse = 1
  sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

  touchpad {
    natural_scroll = false
    tap-to-click = true
  }
}

monitor = eDP-1, 1920x1080@144, 0x0, 1


# autostart      __             __             __
#   ____ ___  __/ /_____  _____/ /_____ ______/ /_
#  / __ `/ / / / __/ __ \/ ___/ __/ __ `/ ___/ __/
# / /_/ / /_/ / /_/ /_/ (__  ) /_/ /_/ / /  / /_
# \__,_/\__,_/\__/\____/____/\__/\__,_/_/   \__/
#
exec-once = zsh $HOME/.config/sway/scripts/import-gsettings.sh &
# waybar
exec-once =  zsh ~/.config/waybar/launch.sh &
#emacs
exec-once =  emacs --daemon &
# startpage
exec = {cd $HOME/.config/startpage && nohup python -m http.server 6868 &}
# background
exec-once =  zsh ~/.azotebg &
# for normal work  xdg-desktop-portal-wlr
exec-once =  dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river &
# Flameshot
exec-once =  flameshot &
# notifications
exec-once =  mako &
# open tablet driver
exec-once =  otd-daemon &
# music play daemon
exec-once =  mpd &
exec-once =  mpDris2 &
# syncthing (for file syncing)
exec-once =  syncthing --no-browser > ~/.config/syncthing/syncthing.log &
# stuff
exec-once =  python ~/.dots/noisy/noisy.py --config ~/.dots/noisy/config.json > ~/.dots/noisy/log &
exec-once =  ~/.config/wayland_scripts/idle.sh &
# polkit
exec-once =  lxsession &
# kde conncect
exec-once =  /usr/lib/kdeconnectd &
exec-once =  indicator-kdeconnect &

exec =  killall nm-applet &
exec =  killall blueman-tray &
exec =  killall blueman-applet &


# decor
#        __                    
#   ____/ /__  _________  _____
#  / __  / _ \/ ___/ __ \/ ___/
# / /_/ /  __/ /__/ /_/ / /    
# \__,_/\___/\___/\____/_/     
decoration {
  rounding = 1

  # Change transparency of focused and unfocused windows
  active_opacity = 1.0
  inactive_opacity = 1.0

  drop_shadow = true
  shadow_range = 4
  shadow_render_power = 3
  col.shadow = rgba(1a1a1aee)

  # https://wiki.hyprland.org/Configuring/Variables/#blur
  blur {
    enabled = false
    size = 3
    passes = 1
    
    vibrancy = 0.1696
  }
}

general {
  gaps_in = 0
  gaps_out = 0
}

animations {
  enabled = true

  # Default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

  # bezier = myBezier, 0.05, 0.9, 0.1, 1.05

  animation = global, 1, 1, default

  # animation = windows, 1, 1, myBezier
  animation = border, 1, 1, default
  animation = borderangle, 1, 1, default
  animation = fade, 1, 1, default
  animation = workspaces, 1, 0.5, default
}
