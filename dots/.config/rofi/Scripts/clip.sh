#!/usr/bin/bash

clip=$(gpaste-client | sed '/^#\|^$\| *#/d')

declare -a options=(
"$clip"
"clear clipboard"
"quit"
)

choise=$(printf '%s\n' "${options[@]}" | sed -r 's/^.{38}//' |rofi -dmenu -i -width 45 -l 20 -p 'Clipboard')


if [[ "$choise" == quit ]]; then
    echo $clip && exit 1

elif [ "$choise"  == "clear clipboard" ]; then
    gpaste-client empty && exit 1

elif [ "$choise" ]; then
    cfg=$(printf '%s\n' "${choise}")
    echo "${cfg}" | head -c -1 | xsel -i -p -b 

else
    echo "OK!" && exit 1
fi

