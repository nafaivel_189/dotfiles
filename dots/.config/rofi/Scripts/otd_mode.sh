#!/usr/bin/bash

declare -a options=(
"drawing"
"osu"
"drawing dual monitor"
"osu dual monitor"
)

choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -width 10 -i -l 4 -p "choose mode:")

if [ "$choise" = "drawing" ]; then
    otd loadsettings $HOME/.dots/devices/tablet_otd/drawing.json
elif [ "$choise" = "osu" ]; then
    otd loadsettings $HOME/.dots/devices/tablet_otd/daily.json
elif [ "$choise" = "drawing dual monitor" ]; then
    otd loadsettings $HOME/.dots/devices/tablet_otd/tablet_settings_dual_monitor.json
elif [ "$choise" = "osu dual monitor" ]; then
    otd loadsettings $HOME/.dots/devices/tablet_otd/osu_tablet_settings_dual_monitor.json
else
    echo "OK!" && exit 1
fi

