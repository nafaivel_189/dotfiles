#!/usr/bin/bash

declare -a options=(
"и i і"
"щ шч šč(sc)"
"(>ᴗ•)"
"(╯°□°)╯︵ ┻━┻"
""
""
)

choise=$(printf '%s\n' "${options[@]}" | $MENU --cache-file /dev/null -dmenu -i -p "Выберы лiтару:")

case $choise in
  "и i і") choise="и";;
  "щ шч šč(sc)") choise="щ";;
esac

# copy="xclip -selection c"
copy="wl-copy"
echo $choise | tr -d "\n" | $copy

