#/bin/bash

note_name=$(wofi --dmenu)

if [ -n "$note_name" ]
then
  note_name=$(echo $note_name | sed 's/ /_/g')
  $GUI_EDITOR "$HOME/Documents/notes/notes_zettelkasten/čyściec/$(date +%Y%m%d)_$note_name.md"
fi


