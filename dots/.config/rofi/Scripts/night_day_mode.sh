#!/usr/bin/bash

declare -a options=(
"2k"
"2.5k"
"3k"
"4k"
"4.5k"
"5k"
"6k"
"off"
)

choise=$(printf '%s\n' "${options[@]}" | $MENU --dmenu -i -p "choose mode:")

killall wlsunset
sleep 0.05 

if [ "$choise" = "4k" ]; then
    wlsunset -t 3999 -T 4000 &
    # bash $HOME/.config/i3/themes/night.sh
    echo "n"
elif [ "$choise" = "4.5k" ]; then
    wlsunset -t 4499 -T 4500 &
    echo "5k"
elif [ "$choise" = "5k" ]; then
    wlsunset -t 4999 -T 5000 &
    echo "5k"
elif [ "$choise" = "2k" ]; then
    wlsunset -t 1999 -T 2000 &
    echo "2k"
elif [ "$choise" = "2.5k" ]; then
    wlsunset -t 2499 -T 2500 &
    echo "2.5k"
elif [ "$choise" = "3k" ]; then
    wlsunset -t 2999 -T 3000 &
    echo "3k"
elif [ "$choise" = "6k" ]; then
    # bash $HOME/.config/i3/themes/day.sh
    wlsunset -t 5999 -T 6000 &
    echo "d"
elif [ "$choise" = "off" ]; then
    # bash $HOME/.config/i3/themes/day_like_night.sh
    echo "dln"
else
    echo "OK!" && exit 1
fi

