#!/usr/bin/bash

declare -a options=(

"quit"
)

choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'virt')

if [[ "$choise" == quit ]]; then
    echo $scripts && exit 1
elif [ "$choise" ]; then
    cfg=$(printf '%s\n' "${choise}" | awk '{print $NF}')
    "$cfg"
else
    echo "OK!" && exit 1
fi

