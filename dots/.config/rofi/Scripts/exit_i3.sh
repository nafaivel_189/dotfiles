#!/usr/bin/bash

declare -a options=(
"i3-msg exit"
"undo"
)

choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -width 10 -i -l 2 -p "exiting i3")

if [[ "$choise" == undo ]]; then
    echo $scripts && exit 1
elif [ "$choise" ]; then
    cfg=$(printf '%s\n' "${choise}" | awk '{print $NF}')
    "$cfg"
else
    echo "OK!" && exit 1
fi

