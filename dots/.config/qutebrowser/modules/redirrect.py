from qutebrowser.api import interceptor, cmdutils, message
from qutebrowser.extensions.interceptors import RedirectException


# If false, disable redirection. Usually changed via the redirect-toggle command
REDIRECT_ENABLED = True


def clean_url(url: str):
    # THINK: should i add any other proto?
    url.removeprefix("https://")
    url.removeprefix("http://")


def upd_url(request: interceptor.Request, new_url: str):
    clean_url(new_url)
    request.request_url.setHost(new_url)
    try:
        request.redirect(request.request_url)
    except RedirectException:
        pass


def rewrite(request: interceptor.Request):
    if not REDIRECT_ENABLED:
        return

    req_url = request.request_url.host()
    clean_url(req_url)

    match req_url:
        case "www.youtube.com":
            # https://github.com/iv-org/invidious
            new_url = "yewtu.be"
            upd_url(request, new_url)

        case "www.reddit.com":
            # https://github.com/libreddit/libreddit
            # https://github.com/libreddit/libreddit-instances/blob/master/instances.md
            new_url = "lr.vern.cc"
            upd_url(request, new_url)

        case "twitter.com" | "x.com":
            # https://github.com/zedeus/nitter
            new_url = "nitter.poast.org"
            upd_url(request, new_url)

        case "stackoverflow.com":  #  | "unix.stackexchange.com":
            # https://github.com/httpjamesm/AnonymousOverflow
            # https://github.com/httpjamesm/AnonymousOverflow/blob/main/instances.json
            new_url = "code.whatever.social"
            upd_url(request, new_url)


interceptor.register(rewrite)


@cmdutils.register()
def redirect_toggle(quiet=False):
    global REDIRECT_ENABLED
    REDIRECT_ENABLED = not REDIRECT_ENABLED
    enabled_str = "enabled" if REDIRECT_ENABLED else "disabled"
    if not quiet:
        message.info("redirrect has been " + enabled_str)
