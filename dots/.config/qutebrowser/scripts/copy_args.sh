#!/bin/bash

if [[ $XDG_SESSION_TYPE == "wayland" ]]; then
copy_cmd="wl-copy"
else
copy_cmd="xsel -b -i"
fi

echo "$@" | tr '\n' ' ' | $copy_cmd
