#!/bin/bash

files=$(sqlite3 -newline '\n' ~/.local/share/zathura/bookmarks.sqlite "SELECT file FROM fileinfo ORDER BY time DESC;")

userexp="\/home\/"$USER

book=$(echo $files | sed --expression='s/\\n/\n/g' | sed --expression="s/$userexp/~/g" --expression='/\/tmp/d' | $MENU -i --dmenu -theme-str 'window {width: 60%;}')

if [ "$book" != "" ]
then
  zathura "$book"
fi

