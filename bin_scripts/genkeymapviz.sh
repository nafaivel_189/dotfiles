#!/bin/bash
keymapviz ~/git_downloads/qmk/qmk/keyboards/crkbd/keymaps/Nafaivel/keymap.c -t fancy -c ~/git_downloads/qmk/qmk/keyboards/crkbd/keymaps/Nafaivel/config.properties \
  | sed \
    --expression="s/\/\*//g" \
    --expression="s/^ \*//g" \
    --expression="/generated/d"
