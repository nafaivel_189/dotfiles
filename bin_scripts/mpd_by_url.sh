#!/usr/bin/env bash
mpc $1 $(yt-dlp --embed-metadata --get-url -f bestaudio $2)
