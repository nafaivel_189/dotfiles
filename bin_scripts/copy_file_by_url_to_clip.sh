#!/usr/bin/env bash
curl "$1" | wl-copy
notify-send -u low "copied" "$1"
