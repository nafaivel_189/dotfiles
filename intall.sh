#/bin/bash
# for dir in ./user/home/$(ls -A $HOME/.dotfiles/user/home/); do ln -s -f -L -i $dir $HOME/ ; done
git clone https://gitlab.com/Nafaivel/nvim_config ./dots/.config/nvim
stow --target=$HOME dots --override=yes --adopt
cp ./fonts/.fonts/* ~/.fonts/ -r
